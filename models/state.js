const mongoose = require("mongoose");
const logger = require("../helpers/logger");

const stateSchema = new mongoose.Schema({
  name: String,
  did: { type: Number, index: true },
  countryId: {type: mongoose.Schema.Types.ObjectId, ref: "Country"}
});

stateSchema.set("toJSON", { virtuals: true });

const StateSchema = mongoose.model("State", stateSchema);
StateSchema.ensureIndexes();
StateSchema.on("index", err => {
  if (err) {
    logger.error(err);
  } else {
    logger.debug("state index done");
  }
});
module.exports = StateSchema;
