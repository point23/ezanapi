const mongoose = require("mongoose");
const logger = require("../helpers/logger");

const timingSchema = new mongoose.Schema({
  dateString: String,
  dateObj: {type: Date, index: true},
  regionId: {type: mongoose.Schema.Types.ObjectId, ref: "Region", index: true},
  imsak: String,
  gunes: String,
  ogle: String,
  ikindi: String,
  aksam: String,
  yatsi: String
});

timingSchema.set("toJSON", { virtuals: true });

const TimingSchema = mongoose.model("Timing", timingSchema);
TimingSchema.ensureIndexes();
TimingSchema.on("index", err => {
  if (err) {
    logger.error(err);
  } else {
    logger.debug("timing index done");
  }
});
module.exports = TimingSchema;
