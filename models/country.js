const mongoose = require("mongoose");
const logger = require("../helpers/logger");

const countrySchema = new mongoose.Schema({
  name: String,
  did: { type: Number, index: true }
});

countrySchema.set("toJSON", { virtuals: true });

const CountrySchema = mongoose.model("Country", countrySchema);
CountrySchema.ensureIndexes();
CountrySchema.on("index", err => {
  if (err) {
    logger.error(err);
  } else {
    logger.debug("country index done");
  }
});
module.exports = CountrySchema;
