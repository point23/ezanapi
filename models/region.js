const mongoose = require("mongoose");
const logger = require("../helpers/logger");

const regionSchema = new mongoose.Schema({
  name: String,
  did: { type: Number, index: true },
  path: String,
  lastParseDate: {type: Date, index: true, default: Date.now()},
  stateId: {type: mongoose.Schema.Types.ObjectId, ref: "State"}
});

regionSchema.set("toJSON", { virtuals: true });

const RegionSchema = mongoose.model("Region", regionSchema);
RegionSchema.ensureIndexes();
RegionSchema.on("index", err => {
  if (err) {
    logger.error(err);
  } else {
    logger.debug("region index done");
  }
});
module.exports = RegionSchema;
