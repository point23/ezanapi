const CountrySchema = require("../models/country");
const RegionSchema = require("../models/region");
const StateSchema = require("../models/state");
const TimingSchema = require("../models/timing");

function prepareQuery(request) {
  const query = {};
  const skipQuery = ["skip", "take", "limit", "page", "sort", "by"];

  for (const key in request) {
    if (request.hasOwnProperty(key)) {
      const element = request[key];
      if (!skipQuery.includes(key)) {
        if (Array.isArray(element)) {
          query[key] = {
            $in: element
          };
        } else {
          query[key] = element;
        }
      }
    }
  }

  return query;
}

exports.index = function(req, res, next) {
  res.locals.data = {};
  next();
};

exports.findCountries = function(req, res, next) {
  CountrySchema.find({})
    .then(countries => {
      const tr = countries.find(c => {
        return c.did == 2;
      });
      const remaining = countries.filter(c => {
        return c.did != 2;
      });
      countries = [tr, ...remaining];
      res.locals.data = countries;
      next();
    })
    .catch(error => {
      res.locals.data = [];
      next();
    });
};

exports.findRegions = function(req, res, next) {
  const query = prepareQuery(req.query);
  RegionSchema.where(query)
    .sort("name")
    .then(regions => {
      res.locals.data = regions;
      next();
    })
    .catch(error => {
      res.locals.data = [];
      next();
    });
};

exports.findStates = function(req, res, next) {
  const query = prepareQuery(req.query);
  StateSchema.where(query)
    .sort("name")
    .then(states => {
      res.locals.data = states;
      next();
    })
    .catch(error => {
      res.locals.data = [];
      next();
    });
};

exports.findTimings = function(req, res, next) {
  const query = prepareQuery(req.query);

  const date = new Date();
  date.setHours(0, 0, 0, 0);

  query.dateObj = { $gte: date };
  TimingSchema.where(query)
    .sort("dateObj")
    .then(timings => {
      res.locals.data = timings;
      next();
    })
    .catch(error => {
      res.locals.data = [];
      next();
    });
};
