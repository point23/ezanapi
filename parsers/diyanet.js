const axios = require("axios");
const cheerio = require("cheerio");
const logger = require("../helpers/logger");
const CountrySchema = require("../models/country");
const StateSchema = require("../models/state");
const RegionSchema = require("../models/region");
const TimingSchema = require("../models/timing");

exports.bootstrap = function() {
  return new Promise(async (resolve, reject) => {
    try {
      logger.debug("checking countries");
      const countries = await CountrySchema.count({});
      if (countries == 0) {
        logger.debug("couldn't find country, seeding");
        await _getCountries();
        logger.debug("country seeding done");
      }

      logger.debug("checking states");
      const states = await StateSchema.count({});
      if (states == 0) {
        logger.debug("couldn't find state, seeding");
        await _getStates();
        logger.debug("state seeding done");
      }

      logger.debug("checking regions");
      const regions = await RegionSchema.count({});
      if (regions == 0) {
        logger.debug("couldn't find regions, seeding");
        await _getRegions();
        logger.debug("regions seeding done");
      }

      resolve(true);
    } catch (error) {
      reject(error);
    }
  });
};

exports.getCountries = function() {
  return _getCountries();
};

exports.getStates = function() {
  return _getStates();
};

exports.getRegions = function() {
  return _getRegions();
};

exports.getPrayerTimes = function() {
  return new Promise(async (resolve, reject) => {
    try {
      /**
       * en son parse edilen regioni al
       * sayfasina gidip gunleri kaydet
       * gunleri kaydederken exist check yap
       */
      const region = await RegionSchema.findOne({}).sort("lastParseDate");
      logger.info(`started getting timings for ${region.name}`);
      if (region) {
        const response = await axios.get(
          `http://namazvakitleri.diyanet.gov.tr${region.path}`
        );
        const $ = cheerio.load(response.data);
        const tableRows = $("#tab-1 tbody tr");
        logger.debug(`${tableRows.length} days found`);
        await tableRows.map(async (index, element) => {
          const columns = $(element).find("td");
          const dateText = $(columns[0]).text();
          const dateParts = dateText.split(" ");

          const exist = await TimingSchema.findOne({
            dateObj: prepareDateObject(
              dateParts[0],
              dateParts[1],
              dateParts[2]
            ),
            regionId: region.id
          });
          if (!exist) {
            const imsak = $(columns[1]).text();
            const gunes = $(columns[2]).text();
            const ogle = $(columns[3]).text();
            const ikindi = $(columns[4]).text();
            const aksam = $(columns[5]).text();
            const yatsi = $(columns[6]).text();
            logger.debug(`creating ${dateText} for ${region.name}`);
            await TimingSchema.create({
              dateString: dateText,
              dateObj: prepareDateObject(
                dateParts[0],
                dateParts[1],
                dateParts[2]
              ),
              regionId: region.id,
              imsak,
              gunes,
              ogle,
              ikindi,
              aksam,
              yatsi
            });
          } else {
            logger.debug(`skipping ${dateText} for ${region.name}`);
          }
        });

        region.lastParseDate = new Date();
        await region.save();
        logger.info("region done");
      }

      resolve();
    } catch (error) {
      logger.info(
        `region errored, saved last parsed date to remove from the queue, ${region.name}`
      );
      reject(error);
    }
  });
};

exports.deleteOldTimes = function() {
  return new Promise(async (resolve, reject) => {
    const filterDate = new Date();
    filterDate.setDate(filterDate.getDate() - 1);
    
    TimingSchema.deleteMany({
      dateObj: { $lt: filterDate }
    })
      .then(result => {
        resolve(result);
      })
      .catch(error => {
        reject(error);
      });
  });
};

function _getCountries() {
  return new Promise((resolve, reject) => {
    axios.get("http://namazvakitleri.diyanet.gov.tr/tr-TR").then(response => {
      try {
        const $ = cheerio.load(response.data);
        const options = $("select[name='country'] option");
        logger.debug(`found ${options.length} countries`);
        options.map(async (index, option) => {
          logger.debug(`looking for ${$(option).text()}`);
          const exist = await CountrySchema.find({
            did: parseInt($(option).val())
          });
          if (!exist || exist.length == 0) {
            logger.debug(`creating ${$(option).text()}`);
            await CountrySchema.create({
              name: $(option).text(),
              did: parseInt($(option).val())
            });
          }
        });
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  });
}

function _getStates() {
  return new Promise(async (resolve, reject) => {
    try {
      const dbCountries = await CountrySchema.find({});
      for (let i = 0; i < dbCountries.length; i++) {
        const country = dbCountries[i];
        const data = await getCountrysStates(country.did);
        logger.debug(`country: ${country.name}, count: ${data.length}`);
        await data.map(async entry => {
          const exist = await StateSchema.findOne({
            did: parseInt(entry.SehirID)
          });
          if (!exist) {
            await StateSchema.create({
              name: entry.SehirAdi,
              did: parseInt(entry.SehirID),
              countryId: country.id
            });
            logger.debug(`yaratildi: ${entry.SehirAdi}`);
          } else {
            logger.debug(`skip ${entry.SehirAdi}`);
          }
        });
      }
      logger.debug("done");
      resolve();
    } catch (error) {
      reject(error);
    }
  });
}

function _getRegions() {
  return new Promise(async (resolve, reject) => {
    try {
      const dbStates = await StateSchema.find({});
      for (let i = 0; i < dbStates.length; i++) {
        const state = dbStates[i];
        const data = await getStatesRegions(state.did);
        logger.debug(`state: ${state.name}, count: ${data.length}`);

        await data.map(async entry => {
          const exist = await StateSchema.findOne({
            did: parseInt(entry.IlceID)
          });
          if (!exist) {
            await RegionSchema.create({
              name: entry.IlceAdi,
              did: parseInt(entry.IlceID),
              path: entry.IlceUrl,
              stateId: state.id
            });
            logger.debug(`yaratildi: ${entry.IlceAdi}`);
          } else {
            logger.debug(`skip ${entry.IlceAdi}`);
          }
        });
      }
      logger.debug("done");
      resolve();
    } catch (error) {
      reject(error);
    }
  });
}

/**
 * diyanetin sitesinden ulkenin statlerini getirir
 * @param {*} countryId
 */
async function getCountrysStates(countryId) {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.get(
        `http://namazvakitleri.diyanet.gov.tr/tr-TR/home/GetRegList?ChangeType=country&CountryId=${countryId}&Culture=tr-TR`
      );
      const data = response.data.StateList;
      resolve(data);
    } catch (error) {
      reject(error);
    }
  });
}

async function getStatesRegions(stateId) {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.get(
        `http://namazvakitleri.diyanet.gov.tr/tr-TR/home/GetRegList?ChangeType=state&Culture=tr-TR&StateId=${stateId}`
      );
      const data = response.data.StateRegionList;
      resolve(data);
    } catch (error) {
      reject(error);
    }
  });
}

function prepareDateObject(day, month, year) {
  const months = [
    "Ocak",
    "Şubat",
    "Mart",
    "Nisan",
    "Mayıs",
    "Haziran",
    "Temmuz",
    "Ağustos",
    "Eylül",
    "Ekim",
    "Kasım",
    "Aralık"
  ];
  const result = new Date();
  result.setFullYear(parseInt(year));
  result.setDate(parseInt(day));
  result.setMonth(months.indexOf(month));
  result.setHours(0, 0, 0, 0);
  return result;
}
