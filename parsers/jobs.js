var schedule = require("node-schedule");
var diyanet = require("./diyanet");

var jobs = [
  {
    schedule: "*/1 * * * *",
    callback: function() {
      diyanet.getPrayerTimes();
    }
  },{
    schedule: "0 0 * * *",
    callback: function() {
      diyanet.deleteOldTimes();
    }
  }
];

exports.scheduleAll = function() {
  jobs.forEach(function(element) {
    element.id = schedule.scheduleJob(element.schedule, element.callback);
  }, this);
};

exports.cancelWithId = function(id) {
  jobs.forEach(function(element) {
    if (element.id == id) {
      id.cancel();
    }
  }, this);
};
