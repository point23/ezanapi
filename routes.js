const express = require("express");
const router = express.Router();
const cache = require("./middlewares/cache");
const apiController = require("./controllers/apiController");

function responseHandler(req, res, next) {
  res.json(res.locals.data);
}

router.use((req, res, next) => {
  req.query.take = req.query.take ? parseInt(req.query.take) : 20;
  req.query.skip = req.query.skip ? parseInt(req.query.skip) : 0;
  next();
});

router.get(
  "/",
  cache.get,
  apiController.index,
  cache.set,
  responseHandler
);

router.get(
  "/countries",
  cache.get,
  apiController.findCountries,
  cache.set,
  responseHandler
);

router.get(
  "/regions",
  cache.get,
  apiController.findRegions,
  cache.set,
  responseHandler
);

router.get(
  "/states",
  cache.get,
  apiController.findStates,
  cache.set,
  responseHandler
);

router.get(
  "/timings",
  cache.get,
  apiController.findTimings,
  cache.set,
  responseHandler
);

module.exports = router;
